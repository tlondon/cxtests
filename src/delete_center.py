class  Delete_Center:
    pathToCenter = None
    centerName = None
    
    def __init__(self, user, password, centerName, pathToCenter):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, '')
    
    def runAll(self):
        try:
            self.deleteCenter()
            self.verifyDeleteCenter()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
            
    def deleteCenter(self):
        #Select center
        HelperClass.selectCenter(self.pathToCenter, self.centerName)
        
        #Delete Center
        sTypeModifyer("f", KeyModifier.ALT) # opens the file dialog
        sType(Key.RIGHT * 6)
        sType(Key.DOWN * 17)
        sType(Key.ENTER)
        sType(Key.TAB)
        sType(Key.SPACE)
    
    #Assert that the delete success message is on the screen
    def verifyDeleteCenter(self):        
        assert wait(imgRoot + "Delete Center\\deleteSuccess.PNG",25)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def tearDown(self):
        sType(Key.SPACE) #Select OK