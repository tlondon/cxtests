class  Manage_Policies:
    section = None
    title = None
    description = None
    value = None
    valueType = None
    
    def __init__(self, user, password, section, title, description, value, valueType):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, '', '', '')
        
    def runAll(self):
        try:
            self.selectManagePolicies()
            self.modifyPolicy()
            self.verifySaved()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectManagePolicies(self):
        sTypeModifyer("f", KeyModifier.ALT) # opens the file dialog
        sType(Key.LEFT * 2)
        sType(Key.DOWN * 3)
        sType(Key.ENTER)
    
    def modifyPolicy(self):
        sType(Key.DOWN * 2)
        
        #Section
        sTypeModifyer("c", KeyModifier.CTRL) #copy
        while(Env.getClipboard().rstrip()!=self.section):
            sType(Key.DOWN)
            sTypeModifyer("c", KeyModifier.CTRL) #copy
            
        sType(Key.RIGHT)
        sType(Key.DOWN)
        
        #Title
        sTypeModifyer("c", KeyModifier.CTRL) #copy
        while(Env.getClipboard().rstrip()!=self.title):
            sType(Key.DOWN)
            sTypeModifyer("c", KeyModifier.CTRL) #copy
            
        sType(Key.RIGHT)
        sType(Key.DOWN)
        
        #Description
        sTypeModifyer("c", KeyModifier.CTRL) #copy
        if Env.getClipboard().rstrip()!=self.description:
            raise Custom_Exception('Line: ' + getLineNumber() + '. Policy Description Did not match: ' + self.description)
        
        #Value
        if self.valueType=='dropdown':
            sType(Key.ADD)
            wait(.5)
            sType(Key.SPACE)
            sType(Key.ADD)
            #r = RunnerSettings.appHandle.focusedWindow()
            #assert click(r.find(self.value))
            assert click(self.value)
            assert click(imgRoot + "Manage Policies\\save.PNG",10)
            HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
        
    def verifySaved(self):        
        p = Pattern(imgRoot + "Manage Policies\\saved.PNG")
        p.similar(.70)
        assert wait(p, 15)
        sType(Key.SPACE)
        sType(Key.TAB)
        sType(Key.SPACE)