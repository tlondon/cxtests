class Test_Customer:
    def __init__(self, user, password, login, oer, roster, enrollment, pathToCenter, centerName, claimMonth, logout):        
        #Run all Login tests
        if login=='1':
            t1 = Login(user, password)
            t1.runAll()
        else:
            RunnerSettings.pathToCenter = pathToCenter
            RunnerSettings.user = user
            RunnerSettings.password = password
            RunnerSettings.centerName = centerName

        #Run all OER tests
        if oer=='1':
            t2 = OER(user, password, centerName, pathToCenter, claimMonth)
            t2.runAll()

        #Run all Roster tests
        if roster=='1':
            t3 = Roster(user, password, centerName, pathToCenter, claimMonth)
            t3.runAll()

        #Test Enrollment Form
        if enrollment=='1':
            t4 = Enrollment_Form(user, password, centerName, pathToCenter, claimMonth)
            t4.runAll()

        #Close CX
        if logout=='1':
            HelperClass.closeApp()