class All_Enrollment:    
    def __init__(self, claimMonth):
        #Test KU
        formsTest = Test_Customer('knowledgel', 'stage', '1', '0', '0', '1', '', 'AL 300065', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'CA', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'CO', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'FL', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'GA', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'IA', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'IL', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'IN', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'KS', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'LA', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'MD', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'MI', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'MN', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'NC', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'NE', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'NM', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'NV', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'NY', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'OH', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'OK', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'OR', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'PA', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'TN', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'TX', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'VA', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'WA', claimMonth, '0')
        formsTest = Test_Customer('knowledgel', 'stage', '0', '0', '0', '1', '', 'WI', claimMonth, '1')

        #Hit the rest of the forms
        formsTest = Test_Customer('SNAP2200', 'stage', '1', '0', '0', '1', '', 'A', claimMonth, '1')#Georgia
        formsTest = Test_Customer('DayCareRes', 'stage', '1', '0', '0', '1', '', 'A', claimMonth, '1')#IL
        formsTest = Test_Customer('TotalCommu', 'stage', '1', '0', '0', '1', '', 'A', claimMonth, '1')#LA
        formsTest = Test_Customer('kathy', 'stage', '1', '0', '0', '1', '', 'A', claimMonth, '1')#NC_2001_Capefear
        formsTest = Test_Customer('Childc', 'stage', '1', '0', '0', '1', '', 'A', claimMonth, '1')#NC_2051_Childc
        formsTest = Test_Customer('nutritions', 'stage', '1', '0', '0', '1', '', 'G', claimMonth, '1')#NC
        formsTest = Test_Customer('SouthernNe', 'stage', '1', '0', '0', '1', '', 'C', claimMonth, '1')#NH
        formsTest = Test_Customer('Childc', 'stage', '1', '0', '0', '1', '', 'NW', claimMonth, '1')#OK_2051_Childc
        formsTest = Test_Customer('KTyer', 'stage', '1', '0', '0', '1', '', 'A', claimMonth, '1')#TX_2839_KTyer
        formsTest = Test_Customer('Childc', 'stage', '1', '0', '0', '1', '', 'Ferry', claimMonth, '1')#VA_2051_Childc
        formsTest = Test_Customer('dongoff', 'stage', '1', '0', '0', '1', '', 'A', claimMonth, '1')#VA
        formsTest = Test_Customer('connielynn', 'stage', '1', '0', '0', '1', '', 'Come', claimMonth, '1')#WV