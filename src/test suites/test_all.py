class Test_All:
    def __init__(self, user, password, pathToCenter, centerName):
        aprilClaimMonth=imgRoot + 'Functions\\claimMonth\\claimMonthApril2013.png'
        julyClaimMonth=imgRoot + 'Functions\\claimMonth\\claimMonthJuly2013.png'
        juneClaimMonth=imgRoot + 'Functions\\claimMonth\\June2013.png'
        AugustClaimMonth=imgRoot + 'Functions\\claimMonth\\August2013.png'

        #Run all Receipts tests
        announce('TEST: Receipts')
        t1 = Receipts(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        #Enroll Center tests
        propertyBag = Enroll_Center_Property_Bag()
        
        announce('TEST: Enroll Center')        
        t1 = Enroll_Center(user, password, propertyBag)
        t1.runAll()

        #Delete Center tests
        announce('TEST: Delete Center')
        t1 = Delete_Center(user, password, '123 Test Center for Tres', imgRoot + "Functions\\center123TestCenterforTres.png")
        t1.runAll()

        #Record attendance/Meal Counts
        propertyBag = Record_Meals_Property_Bag("01")
        propertyBag.useDefaultMealList()
        
        announce('TEST: Record Meals')
        t1 = Record_Meals(user, password, centerName, pathToCenter, juneClaimMonth, propertyBag)
        t1.runAll()

        #Enroll a child
        propertyBag = Enroll_Child_Property_Bag()
        
        announce('TEST: Enroll Child')
        t1 = Enroll_Child(user, password, centerName, pathToCenter, propertyBag)
        t1.runAll()

        #Mark a claim for processing
        announce('TEST: Mark Claims for Processing')
        t1 = Mark_Claims_For_Processing(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        #Test Enrollment Form
        announce('TEST: Enrollment Form')
        t1 = Enrollment_Form(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #Process Claim
        reProcess=1
        printOER=1
        useBlended=1
        t1 = Process_Claim(user, password, centerName, aprilClaimMonth, reProcess, printOER, useBlended)
        t1.runAll()
        
        #Manage Policies
        announce('TEST: Manage Policies')
        section = 'C. Center'
        title = '3. License End Date Edit Check'
        description = 'If the License End Date has been passed, do you want meals to be disallowed as of that End Date, do want to generate a warning, or do you want to generate a warning for 2 or more months before then disallowing?  Default is ignore.		All'
        value = imgRoot + 'Manage Policies\\disallow immediately if.png'
        valueType = 'dropdown'
        t1 = Manage_Policies(user, password, section, title, description, value, valueType)
        t1.runAll()
        
        #Record Center Menus
        month = '04'
        day = '01'
        year = '2013'
        meal = 'Dinner'
        quantity_type = 'actuals'
        propertyBag = Record_Center_Menus_Property_Bag(month, day, year, meal, quantity_type)
        
        announce('TEST: Record_Center_Menus')
        t1 = Record_Center_Menus(user, password, centerName, pathToCenter, aprilClaimMonth, propertyBag)
        t1.runAll()

        #Close CX
        HelperClass.closeApp()