class All_Reports:
    def __init__(self, user, password, pathToCenter, centerName, claimMonth):
        announce('TEST: Center_Claim_Totals')
        t1 = Center_Claim_Totals(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Monthly_Receipt_Totals')
        t1 = Monthly_Receipt_Totals(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Non_Profit_Status_Report')
        t1 = Non_Profit_Status_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Participant_Totals_By_Center')
        t1 = Participant_Totals_By_Center(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Summary_By_FRP')
        t1 = Summary_By_FRP(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: State_Summary')
        t1 = State_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #Attendance
        announce('TEST: Actual_VS_Estimate_Meal_Count_Summary')
        t1 = Actual_VS_Estimate_Meal_Count_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
  
        #5 day
        announce('TEST: Blank_Attendance_Meal_Count_Worksheet')
        t1 = Blank_Attendance_Meal_Count_Worksheet(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #7 Day
        announce('TEST: Blank_Attendance_Meal_Count_Worksheet')
        t1 = Blank_Attendance_Meal_Count_Worksheet('capefear', password, 'GARDEN OF EDEN', '', aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Daily_Attendance_Meal_Count_Report')
        t1 = Daily_Attendance_Meal_Count_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Estimated_Meal_Count_Summary')
        t1 = Estimated_Meal_Count_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: In_Out_Times_Daily_Report')
        t1 = In_Out_Times_Daily_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: In_Out_Times_Weekly_Report')
        t1 = In_Out_Times_Weekly_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Meal_Count_Data_Entry_Times')
        t1 = Meal_Count_Data_Entry_Times(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Meal_Count_Data_Entry_Times')
        t1 = Meal_Count_Data_Entry_Times(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Claimed_Attendance_Only_Report')		
        t1 = Monthly_Claimed_Attendance_Only_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Claimed_Meal_Count_Summary')
        t1 = Monthly_Claimed_Meal_Count_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Claimed_Meal_Counts_By_Age_Group')
        t1 = Monthly_Claimed_Meal_Counts_By_Age_Group(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Claimed_Meal_Counts_By_Child')
        t1 = Monthly_Claimed_Meal_Counts_By_Child(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Paid_Attendance_Only_Report')
        t1 = Monthly_Paid_Attendance_Only_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Paid_Meals_Counts_By_Age_Group')
        t1 = Monthly_Paid_Meals_Counts_By_Age_Group(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Paid_Meals_Counts_Summary')
        t1 = Monthly_Paid_Meals_Counts_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Monthly_Paid_Meals_Counts_Summary')
        t1 = Monthly_Paid_Meals_Counts_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Weekly_Attendance_Meal_Count_One_Class')
        t1 = Weekly_Attendance_Meal_Count_One_Class(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        announce('TEST: Weekly_Attendance_Meal_Count_Report')
        t1 = Weekly_Attendance_Meal_Count_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

        #5 day
        announce('TEST: Weekly_Paid_Attendance_Meal_Counts')
        t1 = Weekly_Paid_Attendance_Meal_Counts(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #7 day
        announce('TEST: Weekly_Paid_Attendance_Meal_Counts')
        t1 = Weekly_Paid_Attendance_Meal_Counts('capefear', password, 'GARDEN OF EDEN', '', aprilClaimMonth)
        t1.runAll()
        
#        #Center
#        announce('TEST: Center_Enrollment_Statistics')
#        t1 = Center_Enrollment_Statistics(user, password, centerName, pathToCenter, aprilClaimMonth)
#        t1.runAll()
        
        announce('TEST: Center_Info_Report')
        t1 = Center_Info_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
#        announce('TEST: Center_List_Export')
#        t1 = Center_List_Export(user, password, centerName, pathToCenter, aprilClaimMonth)
#        t1.runAll()
        
        #Children
        announce('TEST: Roster')
        t1 = Roster(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()

#        announce('TEST: Child_List_Export')
#        t1 = Child_List_Export(user, password, centerName, pathToCenter, aprilClaimMonth)
#        t1.runAll()
        
        announce('TEST: IEF_List')
        t1 = IEF_List(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Verify_FRP_Consistent_Within_Family')
        t1 = Verify_FRP_Consistent_Within_Family(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Child_Racial_Counts_Summary_Per_Center')
        t1 = Child_Racial_Counts_Summary_Per_Center(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Child_Racial_Counts_Summary_All_Centers')
        t1 = Child_Racial_Counts_Summary_All_Centers(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: School_Age_Breakfast_Approval')
        t1 = School_Age_Breakfast_Approval(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Children_Claimed_Without_Abscence')
        t1 = Children_Claimed_Without_Abscence(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Children_Not_Claimed')
        t1 = Children_Not_Claimed(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #Checkboox
        announce('TEST: Year_End_Tax_Summary')
        t1 = Year_End_Tax_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #Claims
        announce('TEST: Twleve_Month_Claims_Summary')
        t1 = Twleve_Month_Claims_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Rate_Summary')
        t1 = Rate_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
#        announce('TEST: Claims_List_Export')
#        t1 = Claims_List_Export(user, password, centerName, pathToCenter, aprilClaimMonth)
#        t1.runAll()
        
        announce('TEST: OER')
        t1 = OER(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Center_Error_Letter')
        t1 = Center_Error_Letter(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #Menus
        announce('TEST: Center_Monthly_Menu_Plan')
        t1 = Center_Monthly_Menu_Plan(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Center_Weekly_Menu')
        t1 = Center_Weekly_Menu(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Center_Weekly_Menu_Infants_Only')
        t1 = Center_Weekly_Menu_Infants_Only(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Centers_Weekly_Menu_NonInfants_Only')
        t1 = Centers_Weekly_Menu_NonInfants_Only(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Daily_Transportion_Log')
        t1 = Daily_Transportion_Log(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Master_Menu_Weekly_Plan')
        t1 = Master_Menu_Weekly_Plan(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Menu_Notes_Report')
        t1 = Menu_Notes_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Menu_Production_Record')
        t1 = Menu_Production_Record(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Menu_Production_Record_Infants_Only')
        t1 = Menu_Production_Record_Infants_Only(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Menu_Production_Record_NonInfants_Only')
        t1 = Menu_Production_Record_NonInfants_Only(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Weekly_Quantities_Required_All_Centers')
        t1 = Weekly_Quantities_Required_All_Centers(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Weekly_Quantities_Required_Per_Center')
        t1 = Weekly_Quantities_Required_Per_Center(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #Receipts
        announce('TEST: Center_Receipts_Journal')
        t1 = Center_Receipts_Journal(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Monthly_Receipts_Totals_Report')
        t1 = Monthly_Receipts_Totals_Report(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Labor_Tally_Sheet')
        t1 = Labor_Tally_Sheet(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        #Reviews
        announce('TEST: Review_History_Status_List')
        t1 = Review_History_Status_List(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
#        announce('TEST: Review_List_Export')
#        t1 = Review_List_Export(user, password, centerName, pathToCenter, aprilClaimMonth)
#        t1.runAll()
        
        announce('TEST: Sponsor_Review_Summary')
        t1 = Sponsor_Review_Summary(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        announce('TEST: Milk_Audit')
        t1 = Milk_Audit(user, password, centerName, pathToCenter, aprilClaimMonth)
        t1.runAll()
        
        t1 = All_Enrollment(claimMonth)
        
        #Close CX
        HelperClass.closeApp()