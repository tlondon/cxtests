class  Mark_Claims_For_Processing:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectMarkCentersClaimsForProcessing()
            self.verifyClaimSubmitted()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectMarkCentersClaimsForProcessing(self):
        sTypeModifyer("f", KeyModifier.ALT) # opens the file dialog
        sType(Key.RIGHT * 6)
        sType(Key.DOWN * 25)
        sType(Key.ENTER)
        sType(Key.SPACE)
        
        #If "This claim has already been submitted. Are you sure you wish to resubmit
        p1 = Pattern(imgRoot + "Mark Claims For Processing\\Resubmit.PNG")
        p1.similar(.85)
        if exists(imgRoot + "Mark Claims For Processing\\Resubmit.PNG",10 ):
            sType(Key.SPACE)
        
        p2 = Pattern(imgRoot + "Mark Claims For Processing\\Inconsistent.PNG")
        p2.similar(.85)
        if exists(p2,10):
            sType(Key.SPACE)
    
    #Assert that the delete success message is on the screen
    def verifyClaimSubmitted(self):        
        p3 = Pattern(imgRoot + "Mark Claims For Processing\\Submitted.PNG")
        p3.similar(.85)
        wait(p3,10)
        sType(Key.SPACE)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)