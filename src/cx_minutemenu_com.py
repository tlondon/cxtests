class CX_MinuteMenu_Com:
        
    def runAll(self):
        try:
            self.openURL()
            self.verifyURL()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def smoke(self):
        self.runAll()
            
    def openURL(self):
        sTypeModifyer("r", KeyModifier.META) # opens the file dialog
        if RunnerSettings.load_config.currentEnvironment=='stage':
            sType('https://stage-cx.minutemenu.com/accounts/register')
        elif RunnerSettings.load_config.currentEnvironment=='prod':
            sType('https://cx.minutemenu.com/accounts/register')
        sType(Key.ENTER)
        
    def verifyURL(self):        
        wait(imgRoot + "CX MinuteMenu Com\\free trial.PNG",25)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def tearDown(self):
        sTypeModifyer("w", KeyModifier.CTRL) #Close browser window
        RunnerSettings.appHandle.focus('Minute Menu CX') #Return Focus to MM