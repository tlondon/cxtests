class Login:
    user = None
    password = None
    
    def __init__(self, user, password):
        HelperClass.initVariables(self,locals().keys(), locals().values())
        self.setRuntimeVariables()

    def runAll(self):
        try:
            self.loginToCX(self.user,self.password)
            self.verifyLogin()
            self.clearRunnerSettings()
        except:
            HelperClass.exceptionHandling(self)
    
    def smoke(self):
        self.runAll()
        
    def setRuntimeVariables(self):
        RunnerSettings.user = self.user
        RunnerSettings.password = self.password
        
    def loginToCX(self,user,password):
        path = RunnerSettings.getPath()
        
        RunnerSettings.appHandle = App.open(path)
        RunnerSettings.appIsOpen=1
        
        assert wait(imgRoot + "Login\\app.PNG",10)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)

        #Login
        sType(user)
        sType(Key.TAB)
        sType(password)
        sType(Key.ENTER)
        
    def verifyLogin(self):        
        waitVanish(imgRoot + 'ScreenCaps\\communicatingWithServers.PNG',20)
        
        assert wait(imgRoot + "Login\\login.PNG",90)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)

        if exists(imgRoot + "Login\\resolution.PNG"):
            sType(Key.SPACE)
            assert wait(imgRoot + "Login\\login.PNG",20)
            HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)

    def clearRunnerSettings(self):
        RunnerSettings.appIsOpen=1
        RunnerSettings.exception=0
        #These should be filled with proper values if possible
#        RunnerSettings.pathToCenter=''
#        RunnerSettings.user=''
#        RunnerSettings.password=''
#        RunnerSettings.centerName=''
#        RunnerSettings.claimMonth = None