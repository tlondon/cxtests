class  Process_Claim:    
    reProcess = None
    printOER = None
    useBlended = None
    
    def __init__(self, user, password, centerName, claimMonth, reProcess, printOER, useBlended):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, '', '', '')
        
    def runAll(self):
        try:
            self.selectProcessingClaims()
            self.processClaim()
            self.verifyProcessClaim()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectProcessingClaims(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claims dialog
        p = Pattern(imgRoot + 'Process Claims\\menuButton.png')
        p.similar(.60)
        click(p,3)

        
    def processClaim(self):
        wait(1)
        waitVanish(imgRoot + 'ScreenCaps\\communicatingWithServers.PNG',240)
        
        #Ensure everything is unchecked
        if exists(imgRoot + 'ScreenCaps\\checked.png'):
            for x in findAll(imgRoot + 'ScreenCaps\\checked.png'):
                click(x)
        
        #Check print OER
        if self.printOER=='1':    
            click(imgRoot + 'Process Claims\\printOER.PNG')
            
        #Check use blended
        if self.useBlended=='1' and exists(imgRoot + 'Process Claims\\blended.PNG'):
            click(imgRoot + 'Process Claims\\blended.PNG')
            
        #Select uncheck all
        click(imgRoot + 'Process Claims\\uncheckAll.PNG')
        
        #Choose process/re-process
        if self.reProcess=='1':
            r = find(imgRoot + 'Process Claims\\reprocess.PNG')
            print r.getX()
            print r.getY()
            print r.getW()
            print r.getH()
            newR = Region(r.getX()-20, r.getY()+2, r.getW()-110, r.getH()-8)
            click(newR)
            waitVanish(imgRoot + 'ScreenCaps\\communicatingWithServers.PNG',120)
        
        sType(Key.TAB*4)
        sTypeModifyer("c", KeyModifier.CTRL) #copy the center name
        
        #Find the row that contains the center name
        i=0
        while self.centerName!=Env.getClipboard():
            sType(Key.DOWN)
            sTypeModifyer("c", KeyModifier.CTRL) #  copy the center name
            if i>999:
                break
            i=i+1
        
        #Catch exception
        if i>999:
            raise Custom_Exception('Unable to find the claim for ' + self.centerName + ' in the Process Claims screen.')
               
        
        shiftTab(3)
        sType(Key.SPACE) #Check the box
        
        click(imgRoot + 'Process Claims\\processButton.PNG')
    
    #Assert that the delete success message is on the screen
    def verifyProcessClaim(self):        
        #Verify the OER came up
        if self.printOER=='1':
            wait(imgRoot + 'Process Claims\\oer.PNG',360)
            closeWindow(1)
            HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
        #Assume that it processed successfully because there is nothing to verify
        else:
            HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
        
        #Close Process Claims window
        p = Pattern(imgRoot + 'Process Claims\\close.PNG')
        p.similar(.60)
        click(p, 240)