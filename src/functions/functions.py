################
## DEPRECATED ##
################

import datetime

def closeWindow(num):
    x=0
    while x<num:
        click(Region(1887,2,16,11))
        wait(3)
        x=x+1
    RunnerSettings.appHandle.focus('Minute Menu CX') #Return Focus to MM

def logTestStatus(classN, error, lineNum, parameters, code, fname, testName):
    passOrFail = -1
    errorShotName = ''
    
    if error=='':
        passOrFail = 1
    #If an error occurred take a screenshot
    else:
        s = str(RunnerSettings.log.runNumber) #store this in a string
        errorShotName = RunnerSettings.load_config.errorScreenShotPath + s + '\\' + str(classN) + '_' + '_' + str(testName) + '_' + str(RunnerSettings.log.runNumber) + '.png'
        HelperClass.takeScreenshot(errorShotName)
        
    testObj = Test(classN, testName, lineNum, parameters, error, code, fname, str(passOrFail), errorShotName)
    RunnerSettings.log.add(testObj)
    
    print ''
    if passOrFail==-1:
        print 'FAIL: ' + str(classN) + '.' + str(testName)
    else:
        print 'PASS: ' + str(classN) + '.' + str(testName)
    print ''

def sType(key):
    wait(RunnerSettings.speed)
    type(key)
    
def sTypeModifyer(key,modifyer):
    wait(RunnerSettings.speed)
    type(key,modifyer)
    
def shiftTab(num):
    x=0
    while x <num:
        wait(RunnerSettings.speed)
        type(Key.TAB, KeyModifier.SHIFT)
        x=x+1
        
def getLineNumber():
    return str(inspect.currentframe().f_lineno)
    
def announce(text):
    print ''
    print text
    print ''