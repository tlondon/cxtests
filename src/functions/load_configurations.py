from javax.xml.xpath import XPath
from javax.xml.xpath import XPathFactory
from org.xml.sax import InputSource

class Load_Configurations:
    devPath = None
    testPath = None
    stagePath = None
    prodPath = None
    pathToSikuli = None
    errorScreenShotPath = None
    currentEnvironment = None
    CXserver = None
    CXport = None
    CXusername = None
    CXpassword = None
    CXdatabase = None
    
    def __init__(self, file):
        try:
            xpath = XPathFactory.newInstance().newXPath()
            inputSource = InputSource(file)
            print str(file)
            self.devPath = xpath.evaluate("//settings//devPath", inputSource)
            self.testPath = xpath.evaluate("//settings//testPath", inputSource)
            self.stagePath = xpath.evaluate("//settings//stagePath", inputSource)
            self.prodPath = xpath.evaluate("//settings//prodPath", inputSource)
            self.pathToSikuli = xpath.evaluate("//settings//pathToSikuli", inputSource)
            self.errorScreenShotPath = xpath.evaluate("//settings//errorScreenShotPath", inputSource)
            self.currentEnvironment = xpath.evaluate("//settings//currentEnvironment", inputSource)
            
            #CX Configuration Settings
            self.CXserver = xpath.evaluate("//settings//CXconnection//db-server", inputSource)
            self.CXport = xpath.evaluate("//settings//CXconnection//db-port", inputSource)
            self.CXusername = xpath.evaluate("//settings//CXconnection//db-username", inputSource)
            self.CXpassword = xpath.evaluate("//settings//CXconnection//db-password", inputSource)
            self.CXdatabase = xpath.evaluate("//settings//CXconnection//db-database", inputSource)
            
            #Load the RunnerSettings class with all the information it needs
            self.populateRunnerSettings()
            
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print str(exc_value)
            
    def populateRunnerSettings(self):
        RunnerSettings.devPath = self.devPath
        RunnerSettings.testPath = self.testPath
        RunnerSettings.stagePath = self.stagePath
        RunnerSettings.prodPath = self.prodPath