class RunnerSettings:
    #Environment Paths - these are not supported yet
    devPath = None
    testPath = None
    stagePath = None
    prodPath = None
            
    #Set's the speed at which keys are typed
    speed=''
    #Bool that identifies if CX is currently open
    appIsOpen=0
    #The application handler
    appHandle = None
    exception=0
    centerMax = 100
    testsHaveStarted=0
    environment = 'stage' #prod, stage, dev, test
    load_config = None #Contains all the XML configuration settings
    connection = None
     
    #These store the most recent config options
    pathToCenter=''
    user=''
    password=''
    centerName=''
    claimMonth = None
    
    #Log object
    log=''
    
    @staticmethod
    def getPath():
        if RunnerSettings.environment=='dev':
            return RunnerSettings.devPath
        elif RunnerSettings.environment=='test':
            return RunnerSettings.testPath
        elif RunnerSettings.environment=='stage':
            return RunnerSettings.stagePath
        elif RunnerSettings.environment=='prod':
            return RunnerSettings.prodPath