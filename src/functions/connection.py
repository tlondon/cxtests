sys.path.append('C:\Program Files\Microsoft SQL Server JDBC 4.0\sqljdbc4.jar')
import com.microsoft.sqlserver.jdbc.SQLServerDriver as Driver
from java.util import Properties

class Connection:
    server = None
    port = None
    username = None
    password = None
    database = None
    
    connObj = None
    
    def __init__(self, server, port, username, password, database):
        self.server = server
        self.port = port
        self.username = username
        self.password = password
        self.database = database
        self.initConnObject()
        
    def initConnObject(self):
        props = Properties()
        props.put('user', self.username)
        props.put('password', self.password)
        props.put('database', self.database)
        
        try:
            self.connObj = Driver().connect('jdbc:sqlserver://' + self.server, props)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            sys.exit(str(exc_value))
            
    #Return result set from a SELECT query
    def getResultSet(self, sql):
        try:
            #scrubbedSQL=str(sql).replace("'", "''")
            print sql
            stmt = self.connObj.createStatement()
            
            #Return result set
            return stmt.executeQuery(sql)
        except:
            import traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            sys.exit(str(exc_value))