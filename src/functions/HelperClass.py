from java.awt import Dimension
from java.awt import Rectangle
from java.awt import Robot
from java.awt import Toolkit
from java.awt.image import BufferedImage
from javax.imageio import ImageIO
from java.io import File

class HelperClass:
    @staticmethod
    #This function inspects the parameters of the constructor and initializes member variables to those values.
    #TODO: Add error checking to validate that a member variable actually exists before assigning.
    def initVariables(obj, keys, values):
        i=0        
        while i<len(keys):
            #print str(keys[i]) + ': ' + str(values[i])
            if str(keys[i])!='self':
                if str(keys[i])=='propertyBag':
                    setattr(obj, str(keys[i]), values[i])
                else:
                    setattr(obj, str(keys[i]), str(values[i]))
            i=i+1
   
    @staticmethod
    def exceptionHandling(obj):
        RunnerSettings.exception=1
        
        #Check how far back the stack goes to get the most helpful error message
        if len(traceback.extract_tb(sys.exc_info()[2]))>1:
            frame = traceback.extract_tb(sys.exc_info()[2])[1]
        else:
            frame = traceback.extract_tb(sys.exc_info()[2])[0]
        
        fname,lineno,fn,text = frame
        exc_type, exc_value, exc_traceback = sys.exc_info()
        
        logTestStatus(obj.__class__.__name__, str(exc_value), lineno, str(vars(obj)), text, fname, fn)
        HelperClass.closeApp()
        
    @staticmethod
    def passLogTestStatus(obj, fileName, functionName):
        logTestStatus(obj.__class__.__name__, '', inspect.currentframe().f_back.f_lineno,str(vars(obj)), '', fileName, functionName)
    
    @staticmethod
    def verifyLogin(user, password):
        #If we are logged in under the wrong credentials
        if user!=RunnerSettings.user:
            t1 = Login(user, password)

        #If the application is not open
        if RunnerSettings.appIsOpen==0 or RunnerSettings.exception==1:
            t1 = Login(RunnerSettings.user, RunnerSettings.password)
            t1.runAll()
            if RunnerSettings.appIsOpen==0:
                raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to login')
        
    @staticmethod
    def initMethod(obj, keys, values, user, password, centerName, pathToCenter, claimMonth):
        try:
            HelperClass.initVariables(obj, keys, values)
            HelperClass.verifyLogin(user, password)
            if centerName!='':
                HelperClass.selectCenter(pathToCenter, centerName)
            if claimMonth!='':
                HelperClass.selectClaimMonth(claimMonth)
        except:
            HelperClass.exceptionHandling(obj)
    
    @staticmethod
    def selectClaimMonth(path):
        waitVanish(imgRoot + 'ScreenCaps\\communicatingWithServers.PNG',120)
        
        #If our required claim month doesn't match the claim month we already have set, change it
        if RunnerSettings.claimMonth != path:
            #Tell my current runnerSettings what my current claim month is
            RunnerSettings.claimMonth = path

            #Select Advance Claim Month from menu
            sTypeModifyer("c", KeyModifier.ALT) # opens the file dialog
            click(imgRoot + 'Functions\\advance claim month.png')

            p = Pattern(path)
            p.similar(.99)
            i=0

            if not exists(p):
                sType(Key.RIGHT)
                sType(Key.SPACE*6) #Set forward just in case
                sType(Key.LEFT)   
                #Iterate through the claim months until one matches
                while (i<=42):
                    if exists(p,0):
                        break
                    #Go to next
                    type(Key.SPACE)
                    i=i+1

                #Log an error if needed
                if i==43:
                    raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to select claim month ' + str(path))
            sType(Key.TAB*2)
            sType(Key.SPACE)
            sType(Key.TAB)
            sType(Key.SPACE)
                       
    @staticmethod
    #Select Center, path is the image path of the center
    def selectCenter(path, centerName):
        if RunnerSettings.centerName != centerName:
            RunnerSettings.centerName = centerName

            #Set focus to Center Region
            click(Region(899,29,20,13))
            click(Region(899,29,20,13))
            
            #Ensure we are at the top
            sType(Key.UP * int(RunnerSettings.centerMax))
            
            scrubbedUser=str(RunnerSettings.user).replace("'", "''")
            sql = "SELECT c.center_name FROM CENTER c WHERE c.CENTER_STATUS_CODE=204 AND client_id=(SELECT client_id FROM [USER] WHERE username='" + scrubbedUser  + "') ORDER BY center_name"
            rs = RunnerSettings.connection.getResultSet(sql)
            
            i = 1
            found = 'false'
            while rs.next():
                if rs.getString("center_name") == centerName:
                    found = 'true'
                    break
                i = i+1
            
            #Log an error if needed
            if found == 'false':
                raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to find center ' + str(centerName))
            
            #Click down the appropriate number of times
            sType(Key.DOWN * i)
            
            #Set focus to Center Region
            click(Region(899,29,20,13))
            click(Region(899,29,20,13))
    
    @staticmethod
    def verifyExcelFormOpened(obj):               
        assert wait(imgRoot + 'ScreenCaps\\Excel.PNG', 180)
        HelperClass.passLogTestStatus(obj, sys.argv[0], sys._getframe().f_code.co_name)
        
    @staticmethod
    def verifyPDFFormOpened(obj):               
        assert wait(imgRoot + 'Enrollment Form\\lots of icons.PNG', 280)
        HelperClass.passLogTestStatus(obj, sys.argv[0], sys._getframe().f_code.co_name)
    
    @staticmethod
    def closeApp():
        try:
            RunnerSettings.appHandle.close()
            RunnerSettings.appIsOpen=0
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print str(exc_type)
            print str(exc_value)
            print str(exc_traceback)
            
    @staticmethod
    def takeScreenshot(fileName):
        s = str(RunnerSettings.log.runNumber) #store this in a string
        
        #Make the directory, this will return false if the directory already exists
        f = File(RunnerSettings.load_config.errorScreenShotPath + s).mkdir();
        
        screenSize = Toolkit.getDefaultToolkit().getScreenSize()
        screenRectangle = Rectangle(screenSize)
        robot = Robot()
        image = robot.createScreenCapture(screenRectangle)
        ImageIO.write(image, 'png', File(fileName))
        
    @staticmethod
    def writeFile(txt):
        curDay = datetime.date.today().strftime("%Y-%m-%d")
        curTime = datetime.datetime.now().strftime("%H-%M-%S")
        time = curDay + ' ' + curTime
        s = str(RunnerSettings.log.runNumber) #store this in a string
        
        path = RunnerSettings.load_config.errorScreenShotPath + s + '\\' + RunnerSettings.centerName + '.txt'
        print path
        
        my_file = open(path,'a')
        my_file.write(txt + '\n')