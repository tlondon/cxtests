#Required Imports
import java.lang.System
import unittest
import os
import sys

#Where my project is stored
root = os.path.dirname((os.getcwd()))
imgRoot= root + "\\images\\" #Where my images are stored
srcRoot= root + "\\src\\" #Where my src is stored

#Load all my files
i=1
for r,d,f in os.walk(root + "\\src"):
    for files in f:
        if (files.endswith(".py") and files!="runner.py" and files!="setup.py" and files!="new_test.py"):
             execfile(os.path.join(r,files))
             print files
        if i==len(f):
            i=1
            break
        i=i+1

#Load Configuration
RunnerSettings.load_config = Load_Configurations(root + '\\configuration.xml')
conn = Connection(RunnerSettings.load_config.CXserver, RunnerSettings.load_config.CXport, RunnerSettings.load_config.CXusername, RunnerSettings.load_config.CXpassword, RunnerSettings.load_config.CXdatabase)
RunnerSettings.connection = conn


#Import Sikuli
java.lang.System.setProperty("sikuli.Home",RunnerSettings.load_config.pathToSikuli)
from sikuli import *

#Settings
Settings.OcrTextSearch = True #to switch on finding text with find("some text")
Settings.OcrTextRead = True # to switch on the Region.text() function
RunnerSettings.speed = .2
RunnerSettings.centerMax = 10 #The number of times to go up when changing centers

#Debug Settings
#RunnerSettings.appIsOpen=1 #Debug setting

#Local Variables
aprilClaimMonth=imgRoot + 'Functions\\claimMonth\\claimMonthApril2013.png'

#######################
##### Unit Tests ######
#######################
#loader = unittest.TestLoader()
#t1_suite = loader.loadTestsFromModule(New_TestCase)
#global_suite = unittest.TestSuite([t1_suite])
##unittest.TextTestRunner(verbosity=2).run(global_suite)
#unittest.TextTestRunner().run(global_suite)

#######################
##### Test Suites #####
#######################
#Perform logging
RunnerSettings.log = Logging()

#Run Entire test suite
#runAll = Test_All('mmdemo','stage',imgRoot+'Functions\\centerTheTresPlace.png', 'The Tres Place')
#allReports = All_Reports('mmdemo','stage',imgRoot+'Functions\\centerTheTresPlace.png', 'The Tres Place', aprilClaimMonth)
#print imgRoot+'Functions\\centerTheTresPlace.png'
oerRoster = Compare_Many_OER_Rosters('mmdemo','stage',imgRoot+'Functions\\centerTheTresPlace.png', 'The Tres Place')

#Verify all enrollment forms come up without error
#runTest = All_Enrollment(aprilClaimMonth)

#Run OER/Roster test
#Test_Customer('knowledgel', 'stage', '1', '1', '1', '0', '', 'AZ', aprilClaimMonth, '1')
     
#Smoke Test
#if RunnerSettings.load_config.currentEnvironment=='stage':
#    runSmoke = Smoke('mmdemo','stage',imgRoot+'Functions\\centerTheTresPlace.png', 'The Tres Place')
#elif RunnerSettings.load_config.currentEnvironment=='prod':
#    runSmoke = Smoke('mmdemo','mm1234',imgRoot+'Functions\\centerTheTresPlace.png', 'The Tres Place')

#Finish logging
RunnerSettings.log.addAllTestRunstoDB()
#######################
#######################
#######################