class Record_Center_Menus:
    propertyBag = None
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth, propertyBag):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
        
    def runAll(self):
        try:
            self.selectRecordCenterMenus()
            self.selectMeal()
            self.populateMeal()
            self.verifyChanges()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def selectRecordCenterMenus(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claim dialog
        sType(Key.RIGHT)
        sType(Key.DOWN * 6)
        sType(Key.ENTER) #opens record center menus
        
    def selectMeal(self):
        sType(self.propertyBag.month)
        sType(Key.RIGHT)
        sType(self.propertyBag.day)
        sType(Key.RIGHT)
        sType(self.propertyBag.year)
        
        sType(Key.TAB)
        
        if self.propertyBag.meal_name=='AMSnack':
            sType(Key.DOWN*1)
        elif self.propertyBag.meal_name=='Lunch':
            sType(Key.DOWN*2)
        elif self.propertyBag.meal_name=='PMSnack':
            sType(Key.DOWN*3)
        elif self.propertyBag.meal_name=='Dinner':
            sType(Key.DOWN*4)
        elif self.propertyBag.meal_name=='EveningSnack':
            sType(Key.DOWN*5)
            
    def populateMeal(self):
        if self.propertyBag.quantity_type=='actuals':
            #Set the Non-Infant Foods
            self.nonInfantsActuals()
            
            #Act upon the 0-3 month old infant field
            self.infants0to3MonthsActuals()
            
            #Infant foods 4-7 Months
            self.infants4to7MonthsActuals()
            
            #Infant foods 8-11 Months
            self.infants8to11MonthsActuals()
            
            sType(Key.TAB)
            sType(Key.SPACE) #Save
    
    def nonInfantsActuals(self):
        sType(Key.TAB*2)
        
        sType(self.propertyBag.meal.NonInfant_Bread_Alt['quantity'])
        sType(Key.TAB)
        if self.propertyBag.meal.NonInfant_Bread_Alt['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(self.propertyBag.meal.NonInfant_Bread_Alt['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(self.propertyBag.meal.NonInfant_Bread_Alt['unitOfMeasure'])
        
        if self.propertyBag.meal_name!='Breakfast':
            sType(Key.TAB*2)
            sType(self.propertyBag.meal.NonInfant_Meat_Alt['quantity'])
            sType(Key.TAB)
            if self.propertyBag.meal.NonInfant_Meat_Alt['unitOfMeasureImageBool']=='1':
                sType(Key.SPACE)
                click(self.propertyBag.meal.NonInfant_Meat_Alt['unitOfMeasure'])
            else:
                sType(Key.UP*30)
                sType(self.propertyBag.meal.NonInfant_Meat_Alt['unitOfMeasure'])
            
            if self.propertyBag.meal_name=='Lunch' or self.propertyBag.meal_name=='Dinner':
                sType(Key.TAB*2)
                sType(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg['quantity'])
                sType(Key.TAB)
                if self.propertyBag.meal.NonInfant_Fruit_Juice_Veg['unitOfMeasureImageBool']=='1':
                    sType(Key.SPACE)
                    click(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg['unitOfMeasure'])
                else:
                    sType(Key.UP*30)
                    sType(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg['unitOfMeasure'])
            
        sType(Key.TAB*2)
        sType(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg2['quantity'])
        sType(Key.TAB)
        if self.propertyBag.meal.NonInfant_Fruit_Juice_Veg2['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg2['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg2['unitOfMeasure'])

        sType(Key.TAB*2)
        sType(self.propertyBag.meal.NonInfant_Milk['quantity'])
        sType(Key.TAB)
        if self.propertyBag.meal.NonInfant_Milk['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(self.propertyBag.meal.NonInfant_Milk['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(self.propertyBag.meal.NonInfant_Milk['unitOfMeasure'])
            
        #Select from the food picker
        sType(Key.TAB)
        sType(Key.SPACE)
        wait(1)
        click(self.propertyBag.meal.NonInfant_Bread_Alt['image'])
        
        if self.propertyBag.meal_name!='Breakfast':
            sType(Key.TAB)
            sType(Key.SPACE)
            wait(1)
            click(self.propertyBag.meal.NonInfant_Meat_Alt['image'])
            
            if self.propertyBag.meal_name=='Lunch' or self.propertyBag.meal_name=='Dinner':
                sType(Key.TAB)
                sType(Key.SPACE)
                wait(1)
                click(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg['image'])
        
        sType(Key.TAB)
        sType(Key.SPACE)
        wait(1)
        click(self.propertyBag.meal.NonInfant_Fruit_Juice_Veg['image'])
        sType(Key.TAB)
        sType(Key.SPACE)
        wait(1)
        click(self.propertyBag.meal.NonInfant_Milk['image'])

        #Move the cursor away
        p = Pattern(imgRoot + 'Record Center Menus\\hover.PNG')
        p.similar(.85)
        hover(p)
            
    def infants0to3MonthsActuals(self):
        #Infant foods 0-3 Months
        sType(Key.TAB*2)
        sType(Key.SPACE)
        wait(1)
        click(self.propertyBag.meal.Infant_03_BreastMilk_Formula['image'])
        sType(Key.TAB)
        sType(self.propertyBag.meal.Infant_03_BreastMilk_Formula['quantity'])
        sType(Key.TAB)
        if self.propertyBag.meal.Infant_03_BreastMilk_Formula['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(self.propertyBag.meal.Infant_03_BreastMilk_Formula['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(self.propertyBag.meal.Infant_03_BreastMilk_Formula['unitOfMeasure'])

        #Move the cursor away
        p = Pattern(imgRoot + 'Record Center Menus\\hover.PNG')
        p.similar(.85)
        hover(p)
        
    def infants4to7MonthsActuals(self):
        if self.propertyBag.meal_name=='Breakfast':
            sType(Key.TAB*3)
        elif self.propertyBag.meal_name=='Lunch' or self.propertyBag.meal_name=='Dinner':
            sType(Key.TAB*4)
        else:
            sType(Key.TAB*2)
            
        sType(self.propertyBag.meal.Infant_47_BreastMilk_Formula['quantity'])        
        sType(Key.TAB)
        if self.propertyBag.meal.Infant_47_BreastMilk_Formula['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(self.propertyBag.meal.Infant_47_BreastMilk_Formula['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(self.propertyBag.meal.Infant_47_BreastMilk_Formula['unitOfMeasure'])

        if self.propertyBag.meal_name!='AMSnack' and self.propertyBag.meal_name!='PMSnack' and self.propertyBag.meal_name!='EveningSnack':
            sType(Key.TAB)
            sType(self.propertyBag.meal.Infant_47_Infant_Cereal['quantity'])
            sType(Key.TAB)
            if self.propertyBag.meal.Infant_47_Infant_Cereal['unitOfMeasureImageBool']=='1':
                sType(Key.SPACE)
                click(self.propertyBag.meal.Infant_47_Infant_Cereal['unitOfMeasure'])
            else:
                sType(Key.UP*30)
                sType(self.propertyBag.meal.Infant_47_Infant_Cereal['unitOfMeasure'])

            if self.propertyBag.meal_name!='Breakfast':
                sType(Key.TAB)
                sType(self.propertyBag.meal.Infant_47_Fruit_Vegtable['quantity'])
                sType(Key.TAB)
                if self.propertyBag.meal.Infant_47_Fruit_Vegtable['unitOfMeasureImageBool']=='1':
                    sType(Key.SPACE)
                    click(self.propertyBag.meal.Infant_47_Fruit_Vegtable['unitOfMeasure'])
                else:
                    sType(Key.UP*30)
                    sType(self.propertyBag.meal.Infant_47_Fruit_Vegtable['unitOfMeasure'])
        
        sType(Key.TAB)
        sType(Key.SPACE)
        wait(1)
        p = Pattern(self.propertyBag.meal.Infant_47_BreastMilk_Formula['image'])
        p.similar(.9)
        click(p)
        
        if self.propertyBag.meal_name!='AMSnack' and self.propertyBag.meal_name!='PMSnack' and self.propertyBag.meal_name!='EveningSnack':
            sType(Key.TAB)
            sType(Key.SPACE)
            wait(1)
            click(self.propertyBag.meal.Infant_47_Infant_Cereal['image'])

            if self.propertyBag.meal_name!='Breakfast':
                sType(Key.TAB)
                sType(Key.SPACE)
                wait(1)
                click(self.propertyBag.meal.Infant_47_Fruit_Vegtable['image'])
        
    def infants8to11MonthsActuals(self):
        if self.propertyBag.meal_name=='AMSnack' or self.propertyBag.meal_name=='PMSnack' or self.propertyBag.meal_name=='EveningSnack':
            second = copy.copy(self.propertyBag.meal.Infant_811_Bread_Alternative)
            third = copy.copy(self.propertyBag.meal.Infant_811_Juice)
        else:
            second = copy.copy(self.propertyBag.meal.Infant_811_Infant_Cereal_Meat_Alternative)
            third = copy.copy(self.propertyBag.meal.Infant_811_Fruit_Vegtable)
                
        sType(Key.TAB*4)
        sType(self.propertyBag.meal.Infant_811_BreastMilk_Formula['quantity'])
        sType(Key.TAB)
        if self.propertyBag.meal.Infant_811_BreastMilk_Formula['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(self.propertyBag.meal.Infant_811_BreastMilk_Formula['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(self.propertyBag.meal.Infant_811_BreastMilk_Formula['unitOfMeasure'])

        sType(Key.TAB)
        sType(second['quantity'])
        sType(Key.TAB)
        if second['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(second['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(second['unitOfMeasure'])

        sType(Key.TAB)
        sType(third['quantity'])

        sType(Key.TAB)#This tab goes out of order for some reason, it's a bug in CX

        sType(Key.SPACE) #Breast Milk
        wait(1)
        p = Pattern(self.propertyBag.meal.Infant_811_BreastMilk_Formula['image'])
        p.similar(.9)
        click(p)

        sType(Key.TAB)#Restore back to the old order
        if third['unitOfMeasureImageBool']=='1':
            sType(Key.SPACE)
            click(third['unitOfMeasure'])
        else:
            sType(Key.UP*30)
            sType(third['unitOfMeasure'])

        sType(Key.TAB)
        sType(Key.SPACE) #Infant Cereal
        p = Pattern(second['image'])
        p.similar(.99)
        wait(2) #It won't find the image without waiting a quick second
        click(p)

        sType(Key.TAB)
        sType(Key.SPACE) #Fruit/Vegtable
        wait(1)
        p = Pattern(third['image'])
        p.similar(.9)
        click(p)
    
#    def populateAMSnack(self):
#    
#    def populateLunch(self):
#        
#    def populatePMSnack(self):
#        
#    def populateDinner(self):
#        
#    def populateEveningSnack(self):
            
    def verifyChanges(self):        
        assert wait(imgRoot + "Record Center Menus\\Save Success.PNG",25)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
    
    def tearDown(self):
        sType(Key.SPACE)
        sType(Key.TAB)
        sType(Key.SPACE)