class  Enroll_Child:
    propertyBag = None
    
    def __init__(self, user, password, centerName, pathToCenter, propertyBag):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, '')
        
    def runAll(self):       
        try:
            self.selectManageChildInformation()
            self.enrollChild()
            self.verifyChildEnrolled()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectManageChildInformation(self):
        sTypeModifyer("f", KeyModifier.ALT) # opens the file dialog
        sType(Key.DOWN * 2);
        sType(Key.ENTER)
        sType(Key.TAB)
        wait(3)
        sType(Key.SPACE)
    
    def enrollChild(self):    
        sType(Key.TAB)
        sType(self.propertyBag.classRoom)
        sType(Key.TAB)
        sType(self.propertyBag.fName)
        sType(Key.TAB)
        sType(self.propertyBag.mInitial)
        sType(Key.TAB)
        sType(self.propertyBag.lName)
        sType(Key.TAB)
        sType(self.propertyBag.bday)
        sType(Key.TAB)
        
        if self.propertyBag.gender=='m':
            sType(Key.DOWN*2)
        else:
            sType(Key.DOWN)
        
        sType(Key.TAB*2)        
        if self.propertyBag.hispanic=='1':
            sType(Key.SPACE)#hispanic
            sType(Key.TAB)
            sType(Key.TAB)
        else:
            sType(Key.TAB)
            sType(Key.SPACE)#hispanic
            sType(Key.TAB)
        
        if self.propertyBag.indian=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.asian=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.black=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.pacific=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.white=='1':
            sType(Key.SPACE)
        
        sType(Key.TAB)
        sType(self.propertyBag.originalEnrollment)
        sType(Key.TAB)
        sType(self.propertyBag.currentEnrollment)
        sType(Key.TAB)
        sType(self.propertyBag.enrollmentExpiration)
        sType(Key.TAB*16)
        
        #Days of the week times
        if self.propertyBag.monIn!="":
            sType(self.propertyBag.monIn)
        sType(Key.TAB)
        if self.propertyBag.monOut!="":
            sType(self.propertyBag.monOut)
        sType(Key.TAB*4)
        
        if self.propertyBag.tueIn!="":
            sType(self.propertyBag.tueIn)
        sType(Key.TAB)
        if self.propertyBag.tueOut!="":
            sType(self.propertyBag.tueOut)
        sType(Key.TAB*4)
        
        if self.propertyBag.wedIn!="":
            sType(self.propertyBag.wedIn)
        sType(Key.TAB)
        if self.propertyBag.wedOut!="":
            sType(self.propertyBag.wedOut)
        sType(Key.TAB*4)
        
        if self.propertyBag.thurIn!="":
            sType(self.propertyBag.thurIn)
        sType(Key.TAB)
        if self.propertyBag.thurOut!="":
            sType(self.propertyBag.thurOut)
        sType(Key.TAB*4)
        
        if self.propertyBag.friIn!="":
            sType(self.propertyBag.friIn)
        sType(Key.TAB)
        if self.propertyBag.friOut!="":
            sType(self.propertyBag.friOut)
            
        sType(Key.TAB*15)
        
        #Meals
        if self.propertyBag.B=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.A=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.L=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.P=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.D=='1':
            sType(Key.SPACE)
        sType(Key.TAB)
        
        if self.propertyBag.E=='1':
            sType(Key.SPACE)
        
        sType(Key.TAB*15)
        sType(Key.SPACE)#NEXT
        
        #verify a duplicate child 
        self.verifyDuplicateChild()
        
        #Choose guardian
        sType(Key.DOWN*(1+int(self.propertyBag.guardian)))
        sType(Key.TAB*31)
        sType(Key.SPACE)#NEXT
        
        #Choose FRP
        shiftTab(6)
        if self.propertyBag.FRP=='F':
            sType(Key.DOWN)
        elif self.propertyBag.FRP=='R':
            sType(Key.DOWN*2)
        elif self.propertyBag.FRP=='P':
            sType(Key.DOWN*3)
        
        #Save
        shiftTab(3)
        sType(Key.SPACE)
        
        wait(3)
        
        if exists(imgRoot + "Enroll Child\\doesNotExists.PNG"):
            sType(Key.SPACE)
        
    def verifyChildEnrolled(self):        
        assert wait(imgRoot + "Enroll Child\\newChildEnrolled.PNG", 5)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
                
    def verifyDuplicateChild(self):        
        if exists(imgRoot + "ScreenCaps\\warningwarningwarning.PNG",3):
            raise Custom_Exception('Line: ' + getLineNumber() + '. Duplicate Child Exists')
            
    def tearDown(self):
        sType(Key.SPACE)
        sType(Key.TAB)
        sType(Key.SPACE)