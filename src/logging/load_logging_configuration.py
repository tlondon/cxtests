from javax.xml.xpath import XPath
from javax.xml.xpath import XPathFactory
from org.xml.sax import InputSource

class Load_Logging_Configuration:
    LoggingConnection = None
    
    def __init__(self, file):
        try:
            xpath = XPathFactory.newInstance().newXPath()
            inputSource = InputSource(file)
            
            server = xpath.evaluate("//connection//db-server", inputSource)
            port = xpath.evaluate("//connection//db-port", inputSource)
            username = xpath.evaluate("//connection//db-username", inputSource)
            password = xpath.evaluate("//connection//db-password", inputSource)
            database = xpath.evaluate("//connection//db-database", inputSource)
            
            self.LoggingConnection = Connection(server, port, username, password, database)
            
        except XPathExpressionException:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print str(exc_value)