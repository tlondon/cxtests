CREATE DATABASE CXTests
use CXTests
CREATE TABLE individualRuns
(
	individualRuns_id int IDENTITY(1,1) PRIMARY KEY,
	class_name varchar(100),
	test_name varchar(100),
	runNumber bigint NOT NULL,
	lineNumber int,
	errorMsg text,
	code text,
	errorShotName text,
	fname text,
	parameterList text,
	timeOfRun datetime,
	passFailNotRun int
)
CREATE TABLE runs
(
	run_id int IDENTITY(1,1) PRIMARY KEY,
	runNumber bigint NOT NULL,
	start_time datetime,
	end_time datetime
)