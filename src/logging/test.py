class Test:
    classN = None #Remove this, it's only here because initVariables relies on it
    theClass = None
    testName = None
    error = None
    passOrFail = None
    parameters = None
    lineNum = None
    code = None
    fname = None
    time = None
    errorShotName = None
    
    def __init__(self, theClass, testName, lineNum, parameters, error, code, fname, passOrFail, errorShotName):
        HelperClass.initVariables(self,locals().keys(), locals().values())
        curDay = datetime.date.today().strftime("%Y-%m-%d")
        curTime = datetime.datetime.now().strftime("%H:%M:%S")
        self.time = curDay + ' ' + curTime