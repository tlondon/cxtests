import collections
sys.path.append('C:\Program Files\Microsoft SQL Server JDBC 4.0\sqljdbc4.jar')
import com.microsoft.sqlserver.jdbc.SQLServerDriver as Driver
from java.util import Properties

class Logging:        
    #2D array that contains a list of all test and whether they passed or failed
    #If they failed, it contains it's error message
    tests = None
    testsToErrorMapping = None
    conn = None
    curDay = None
    curTime = None
    dayOftheWeek = None
    dayOfWeekMonthAndYear = None
    dateOfRun = None
    runNumber = None
    
    def __init__(self):            
        self.connect()
        self.tests = collections.defaultdict(list)
        self.testsToErrorMapping = collections.defaultdict(list)
        
        self.curDay = datetime.date.today().strftime("%Y-%m-%d")
        self.curTime = datetime.datetime.now().strftime("%H:%M:%S")
        self.dateOfRun=self.curDay + ' ' + self.curTime
        self.runNumber = millis = int(round(time.time()))
        self.testRuns = list()
    
    def connect(self):
        loggingConfig = Load_Logging_Configuration(root + '\\src\\logging\\loggingConfig.xml')
        props = Properties()
        props.put('user', loggingConfig.LoggingConnection.username)
        props.put('password', loggingConfig.LoggingConnection.password)
        props.put('database', loggingConfig.LoggingConnection.database)
        
        try:
            self.conn = Driver().connect('jdbc:sqlserver://' + loggingConfig.LoggingConnection.server, props)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            sys.exit(str(exc_value))
            
    def add(self,testObj):
        try:
            error=str(testObj.error).replace("'", "`")
            fixedParameters=str(testObj.parameters).replace("'", "`")
            fixedCode = str(testObj.code).replace("'", "`")
            fixedErrorShotName = str(testObj.errorShotName).replace("'", "`")
            
            sql = 'INSERT INTO individualRuns VALUES (\'' + str(testObj.theClass) + '\',\'' + str(testObj.testName) + '\',\'' + str(self.runNumber) + '\',\'' + testObj.lineNum + '\',\'' + str(error) + '\',\'' + fixedCode + '\',\'' + fixedErrorShotName + '\',\'' + testObj.fname + '\',\'' + str(fixedParameters) + '\',\'' + testObj.time + '\',' + testObj.passOrFail + ')'
            print sql
            stmt = self.conn.createStatement()
            stmt.executeUpdate(sql)
        except:
            import traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            sys.exit(str(exc_value))
    
    def addAllTestRunstoDB(self):
        try:            
            end_time = datetime.date.today().strftime("%Y-%m-%d") + ' ' + datetime.datetime.now().strftime("%H:%M:%S")
            sql = 'INSERT INTO runs VALUES (\'' + str(self.runNumber) + '' + '\',\'' + str(self.dateOfRun) + '\',\'' + str(end_time) + '\')'
            stmt = self.conn.createStatement()
            stmt.executeUpdate(sql)
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            sys.exit(str(exc_value))