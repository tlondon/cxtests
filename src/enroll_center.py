class  Enroll_Center:
    propertyBag = None
    
    def __init__(self, user, password, propertyBag):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, '', '', '')
        
    def runAll(self):
        try:
            self.enrollCenter()
            self.verifyEnrollCenter()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def enrollCenter(self):    
        click(imgRoot + "Enroll Center\\enroll_center.PNG")
        wait(5)
        sType(Key.TAB)
        sType(self.propertyBag.cNum) #Center#
        sType(Key.TAB)
        sType(self.propertyBag.cName) #Center Name
        sType(Key.TAB*29)
        sType(self.propertyBag.oStartDate);#Original Start Date
        sType(Key.TAB*13)
        sType(Key.SPACE)#Next
        sType(Key.TAB*7)
        sType(Key.DOWN*int(self.propertyBag.license));#Choose License Type
        sType(Key.TAB*4);
        sType(Key.BACKSPACE);#Choose Capacity
        sType(self.propertyBag.capacity);#Choose Capacity
        sType(Key.TAB*10);
        
        if self.propertyBag.Cb==1:
            sType(Key.SPACE)#Breakfast
        
        sType(Key.TAB)    
        if self.propertyBag.Ca==1:
            sType(Key.SPACE)#AM Snack
            
        sType(Key.TAB)    
        if self.propertyBag.Cl==1:
            sType(Key.SPACE)#Lunch
        
        sType(Key.TAB)
        if self.propertyBag.Cp==1:
            sType(Key.SPACE)#PM Snack
            
        sType(Key.TAB)    
        if self.propertyBag.Cd==1:
            sType(Key.SPACE)#Dinner
        
        sType(Key.TAB)   
        if self.propertyBag.Ce==1:
            sType(Key.SPACE)#Evening Snack
            
        sType(Key.TAB*3)
            
        if self.propertyBag.Ab==1:
            sType(Key.SPACE)#Breakfast
        
        sType(Key.TAB)    
        if self.propertyBag.Aa==1:
            sType(Key.SPACE)#AM Snack
            
        sType(Key.TAB)    
        if self.propertyBag.Al==1:
            sType(Key.SPACE)#Lunch
        
        sType(Key.TAB)    
        if self.propertyBag.Ap==1:
            sType(Key.SPACE)#PM Snack
            
        sType(Key.TAB)
        if self.propertyBag.Ad==1:
            sType(Key.SPACE)#Dinner
        
        sType(Key.TAB)
        if self.propertyBag.Ae==1:
            sType(Key.SPACE)#Evening Snack
                
        #Determine how many days we are open
        #Add all the meal times to my list
        mealTimes = []
        count = 0
        if self.propertyBag.Ce==1 or self.propertyBag.Ae==1:
            count=count+1
            mealTimes.append(self.propertyBag.Eend)
            mealTimes.append(self.propertyBag.EStart)
        if self.propertyBag.Cd==1 or self.propertyBag.Ad==1:
            count=count+1
            mealTimes.append(self.propertyBag.DEnd)
            mealTimes.append(self.propertyBag.DStart)
        if self.propertyBag.Cp==1 or self.propertyBag.Ap==1:
            count=count+1
            mealTimes.append(self.propertyBag.PMEnd)
            mealTimes.append(self.propertyBag.PMStart)
        if self.propertyBag.Cl==1 or self.propertyBag.Al==1:
            count=count+1
            mealTimes.append(self.propertyBag.LEnd)
            mealTimes.append(self.propertyBag.LStart)
        if self.propertyBag.Ca==1 or self.propertyBag.Aa==1:
            count=count+1
            mealTimes.append(self.propertyBag.AEnd)
            mealTimes.append(self.propertyBag.AStart)
        if self.propertyBag.Cb==1 or self.propertyBag.Ab==1:
            count=count+1
            mealTimes.append(self.propertyBag.BEnd)
            mealTimes.append(self.propertyBag.BStart)
        
        sType(Key.TAB)
        
        #Iterate through the number of meals server
        i=0
        while i<count:
            sType(mealTimes.pop())
            sType(Key.TAB)
            sType(mealTimes.pop())
            sType(Key.TAB)
            i=i+1
        
        sType(Key.TAB*14)
        
        #Choose days open
        if self.propertyBag.Mon!=1:
            sType(Key.SPACE)#Monday
        sType(Key.TAB)
        
        if self.propertyBag.Tue!=1:
            sType(Key.SPACE)#Tuesday
        sType(Key.TAB)
        
        if self.propertyBag.Wed!=1:
            sType(Key.SPACE)#Wednesday
        sType(Key.TAB)
        
        if self.propertyBag.Thur!=1:
            sType(Key.SPACE)#Thursday
        sType(Key.TAB)
        
        if self.propertyBag.Fri!=1:
            sType(Key.SPACE)#Friday
        sType(Key.TAB)
        
        if self.propertyBag.Sat==1:
            sType(Key.SPACE)#Saturday
        sType(Key.TAB)
        
        if self.propertyBag.Sun==1:
            sType(Key.SPACE)#Sunday
        
        sType(Key.TAB)
        sType(Key.SPACE)#Next
        
        sType(Key.TAB*19)
        sType(Key.SPACE)#Save
        
    def verifyEnrollCenter(self):        
        assert wait(imgRoot + "Enroll Center\\success.PNG",5)
        sType(Key.SPACE)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def tearDown(self):
        assert click(imgRoot + "Enroll Center\\close.PNG")