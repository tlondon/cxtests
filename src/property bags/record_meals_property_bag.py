class ChildMeal:
    childNum = None
    B = None
    A = None
    L = None
    P = None
    D = None
    E = None
    
    def __init__(self, childNum, B, A, L, P, D, E):
        self.childNum = childNum
        self.B = B
        self.A = A
        self.L = L
        self.P = P
        self.D = D
        self.E = E
        

class  Record_Meals_Property_Bag:
    mealList = list()
    dayOfMonth = None
    
    def __init__(self, dayOfMonth):
        self.dayOfMonth=dayOfMonth
    
    def addMeal(self, mealForChild):
        self.mealList.append(mealForChild)
        
    def getMealList(self):
        return self.mealList
    
    def useDefaultMealList(self):
        t1 = ChildMeal(1, 1, 0, 1, 0, 0, 0)
        t2 = ChildMeal(2, 1, 0, 1, 0, 0, 0)
        t3 = ChildMeal(3, 1, 0, 1, 0, 0, 0)
        self.addMeal(t1)
        self.addMeal(t2)
        self.addMeal(t3)