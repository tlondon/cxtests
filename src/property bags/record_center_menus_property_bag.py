import copy

class Meal:
    NonInfant_Bread_Alt = dict()
    NonInfant_Bread_Alt['image'] = None
    NonInfant_Bread_Alt['unitOfMeasureImageBool'] = None
    NonInfant_Bread_Alt['quantity'] = None
    NonInfant_Bread_Alt['unitOfMeasure'] = None
    
    NonInfant_Meat_Alt = dict()
    NonInfant_Meat_Alt['image'] = None
    NonInfant_Meat_Alt['unitOfMeasureImageBool'] = None
    NonInfant_Meat_Alt['quantity'] = None
    NonInfant_Meat_Alt['unitOfMeasure'] = None
    
    NonInfant_Fruit_Juice_Veg = dict()
    NonInfant_Fruit_Juice_Veg['image'] = None
    NonInfant_Fruit_Juice_Veg['unitOfMeasureImageBool'] = None
    NonInfant_Fruit_Juice_Veg['quantity'] = None
    NonInfant_Fruit_Juice_Veg['unitOfMeasure'] = None
    
    NonInfant_Fruit_Juice_Veg2= dict()
    NonInfant_Fruit_Juice_Veg2['image'] = None
    NonInfant_Fruit_Juice_Veg2['unitOfMeasureImageBool'] = None
    NonInfant_Fruit_Juice_Veg2['quantity'] = None
    NonInfant_Fruit_Juice_Veg2['unitOfMeasure'] = None
    
    NonInfant_Milk = dict()
    NonInfant_Milk['image'] = None
    NonInfant_Milk['unitOfMeasureImageBool'] = None
    NonInfant_Milk['quantity'] = None
    NonInfant_Milk['unitOfMeasure'] = None
    
    Infant_03_BreastMilk_Formula = dict()
    Infant_03_BreastMilk_Formula['image'] = None
    Infant_03_BreastMilk_Formula['unitOfMeasureImageBool'] = None
    Infant_03_BreastMilk_Formula['quantity'] = None
    Infant_03_BreastMilk_Formula['unitOfMeasure'] = None
    
    Infant_47_BreastMilk_Formula = dict()
    Infant_47_BreastMilk_Formula['image'] = None
    Infant_47_BreastMilk_Formula['unitOfMeasureImageBool'] = None
    Infant_47_BreastMilk_Formula['quantity'] = None
    Infant_47_BreastMilk_Formula['unitOfMeasure'] = None
    
    Infant_47_Infant_Cereal = dict()
    Infant_47_Infant_Cereal['image'] = None
    Infant_47_Infant_Cereal['unitOfMeasureImageBool'] = None
    Infant_47_Infant_Cereal['quantity'] = None
    Infant_47_Infant_Cereal['unitOfMeasure'] = None
    
    Infant_47_Fruit_Vegtable = dict()
    Infant_47_Fruit_Vegtable['image'] = None
    Infant_47_Fruit_Vegtable['unitOfMeasureImageBool'] = None
    Infant_47_Fruit_Vegtable['quantity'] = None
    Infant_47_Fruit_Vegtable['unitOfMeasure'] = None
    
    Infant_811_BreastMilk_Formula = dict()
    Infant_811_BreastMilk_Formula['image'] = None
    Infant_811_BreastMilk_Formula['unitOfMeasureImageBool'] = None
    Infant_811_BreastMilk_Formula['quantity'] = None
    Infant_811_BreastMilk_Formula['unitOfMeasure'] = None
    
    Infant_811_Infant_Cereal_Meat_Alternative = dict()
    Infant_811_Infant_Cereal_Meat_Alternative['image'] = None
    Infant_811_Infant_Cereal_Meat_Alternative['unitOfMeasureImageBool'] = None
    Infant_811_Infant_Cereal_Meat_Alternative['quantity'] = None
    Infant_811_Infant_Cereal_Meat_Alternative['unitOfMeasure'] = None
    
    Infant_811_Fruit_Vegtable = dict()
    Infant_811_Fruit_Vegtable['image'] = None
    Infant_811_Fruit_Vegtable['unitOfMeasureImageBool'] = None
    Infant_811_Fruit_Vegtable['quantity'] = None
    Infant_811_Fruit_Vegtable['unitOfMeasure'] = None
    
    Infant_811_Bread_Alternative = dict()
    Infant_811_Bread_Alternative['image'] = None
    Infant_811_Bread_Alternative['unitOfMeasureImageBool'] = None
    Infant_811_Bread_Alternative['quantity'] = None
    Infant_811_Bread_Alternative['unitOfMeasure'] = None
    
    Infant_811_Juice = dict()
    Infant_811_Juice['image'] = None
    Infant_811_Juice['unitOfMeasureImageBool'] = None
    Infant_811_Juice['quantity'] = None
    Infant_811_Juice['unitOfMeasure'] = None
    
class  Record_Center_Menus_Property_Bag:
    month = None
    day = None
    year = None
    meal_name = None
    quantity_type = None
    meal = None
    
    #Create a default breakfast
    breakfast = Meal()
    
    breakfast.NonInfant_Bread_Alt['image'] = imgRoot + 'Record Center Menus\\Food\\Oatmeal - Instant.PNG'
    breakfast.NonInfant_Fruit_Juice_Veg['image'] = imgRoot + 'Record Center Menus\\Food\\Apple Juice.PNG'
    breakfast.NonInfant_Milk['image'] = imgRoot + 'Record Center Menus\\Food\\Fluid Milk.PNG'
    breakfast.Infant_03_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    breakfast.Infant_47_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    breakfast.Infant_47_Infant_Cereal['image'] = imgRoot + 'Record Center Menus\\Food\\Infant Rice Cereal.PNG'
    breakfast.Infant_811_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    breakfast.Infant_811_Infant_Cereal_Meat_Alternative['image'] = imgRoot + 'Record Center Menus\\Food\\Infant Rice Cereal.PNG'
    breakfast.Infant_811_Fruit_Vegtable['image'] = imgRoot + 'Record Center Menus\\Food\\Apples.PNG'
    
    breakfast.NonInfant_Bread_Alt['quantity'] = '1'
    breakfast.NonInfant_Fruit_Juice_Veg['quantity'] = '1'
    breakfast.NonInfant_Milk['quantity'] = '1'
    breakfast.Infant_03_BreastMilk_Formula['quantity'] = '1'
    breakfast.Infant_47_BreastMilk_Formula['quantity'] = '1'
    breakfast.Infant_47_Infant_Cereal['quantity'] = '1'
    breakfast.Infant_811_BreastMilk_Formula['quantity'] = '1'
    breakfast.Infant_811_Infant_Cereal_Meat_Alternative['quantity'] = '1'
    breakfast.Infant_811_Fruit_Vegtable['quantity'] = '1'
    
    breakfast.NonInfant_Bread_Alt['unitOfMeasure'] = 'ounces'
    breakfast.NonInfant_Fruit_Juice_Veg['unitOfMeasure'] = 'half pints'
    breakfast.NonInfant_Milk['unitOfMeasure'] = 'cups'
    breakfast.Infant_03_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    breakfast.Infant_47_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    breakfast.Infant_47_Infant_Cereal['unitOfMeasure'] = 'servings'
    breakfast.Infant_811_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    breakfast.Infant_811_Infant_Cereal_Meat_Alternative['unitOfMeasure'] = 'servings'
    breakfast.Infant_811_Fruit_Vegtable['unitOfMeasure'] = 'slices'
    
    breakfast.NonInfant_Bread_Alt['unitOfMeasureImageBool'] = '0'
    breakfast.NonInfant_Fruit_Juice_Veg['unitOfMeasureImageBool'] = '0'
    breakfast.NonInfant_Milk['unitOfMeasureImageBool'] = '0'
    breakfast.Infant_03_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    breakfast.Infant_47_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    breakfast.Infant_47_Infant_Cereal['unitOfMeasureImageBool'] = '0'
    breakfast.Infant_811_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    breakfast.Infant_811_Infant_Cereal_Meat_Alternative['unitOfMeasureImageBool'] = '0'
    breakfast.Infant_811_Fruit_Vegtable['unitOfMeasureImageBool'] = '0'
    
    #Create a default AMSnack
    snack = Meal()

    snack.NonInfant_Bread_Alt['image'] = imgRoot + 'Record Center Menus\\Food\\Oatmeal - Instant.PNG'
    snack.NonInfant_Meat_Alt['image'] = imgRoot + 'Record Center Menus\\Food\\Pork Chops.PNG'
    snack.NonInfant_Fruit_Juice_Veg['image'] = imgRoot + 'Record Center Menus\\Food\\Apple Juice.PNG'
    snack.NonInfant_Milk['image'] = imgRoot + 'Record Center Menus\\Food\\Fluid Milk.PNG'
    snack.Infant_03_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    snack.Infant_47_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    snack.Infant_811_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    snack.Infant_811_Bread_Alternative['image'] = imgRoot + 'Record Center Menus\\Food\\Infant Rice Cereal.PNG'
    snack.Infant_811_Juice['image'] = imgRoot + 'Record Center Menus\\Food\\Apple Juice.PNG'

    snack.NonInfant_Bread_Alt['quantity'] = '1'
    snack.NonInfant_Meat_Alt['quantity'] = '1'
    snack.NonInfant_Fruit_Juice_Veg['quantity'] = '1'
    snack.NonInfant_Milk['quantity'] = '1'
    snack.Infant_03_BreastMilk_Formula['quantity'] = '1'
    snack.Infant_47_BreastMilk_Formula['quantity'] = '1'
    snack.Infant_811_BreastMilk_Formula['quantity'] = '1'
    snack.Infant_811_Bread_Alternative['quantity'] = '1'
    snack.Infant_811_Juice['quantity'] = '1'

    snack.NonInfant_Bread_Alt['unitOfMeasure'] = 'ounces'
    snack.NonInfant_Meat_Alt['unitOfMeasure'] = 'ounces'
    snack.NonInfant_Fruit_Juice_Veg['unitOfMeasure'] = 'half pints'
    snack.NonInfant_Milk['unitOfMeasure'] = 'cups'
    snack.Infant_03_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    snack.Infant_47_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    snack.Infant_811_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    snack.Infant_811_Bread_Alternative['unitOfMeasure'] = 'servings'
    snack.Infant_811_Juice['unitOfMeasure'] = 'cups'

    snack.NonInfant_Bread_Alt['unitOfMeasureImageBool'] = '0'
    snack.NonInfant_Meat_Alt['unitOfMeasureImageBool'] = '0'
    snack.NonInfant_Fruit_Juice_Veg['unitOfMeasureImageBool'] = '0'
    snack.NonInfant_Milk['unitOfMeasureImageBool'] = '0'
    snack.Infant_03_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    snack.Infant_47_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    snack.Infant_811_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    snack.Infant_811_Bread_Alternative['unitOfMeasureImageBool'] = '0'
    snack.Infant_811_Juice['unitOfMeasureImageBool'] = '0'
    
    #Create a default Lunch
    lunchDinner = Meal()

    lunchDinner.NonInfant_Bread_Alt['image'] = imgRoot + 'Record Center Menus\\Food\\Oatmeal - Instant.PNG'
    lunchDinner.NonInfant_Meat_Alt['image'] = imgRoot + 'Record Center Menus\\Food\\Pork Chops.PNG'
    lunchDinner.NonInfant_Fruit_Juice_Veg['image'] = imgRoot + 'Record Center Menus\\Food\\Apple Juice.PNG'
    lunchDinner.NonInfant_Fruit_Juice_Veg2['image'] = imgRoot + 'Record Center Menus\\Food\\Apple Juice.PNG'
    lunchDinner.NonInfant_Milk['image'] = imgRoot + 'Record Center Menus\\Food\\Fluid Milk.PNG'
    lunchDinner.Infant_03_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    lunchDinner.Infant_47_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    lunchDinner.Infant_47_Infant_Cereal['image'] = imgRoot + 'Record Center Menus\\Food\\Infant Rice Cereal.PNG'
    lunchDinner.Infant_47_Fruit_Vegtable['image'] = imgRoot + 'Record Center Menus\\Food\\Apples.PNG'
    lunchDinner.Infant_811_BreastMilk_Formula['image'] = imgRoot + 'Record Center Menus\\Food\\Breast Milk.PNG'
    lunchDinner.Infant_811_Infant_Cereal_Meat_Alternative['image'] = imgRoot + 'Record Center Menus\\Food\\Infant Rice Cereal.PNG'
    lunchDinner.Infant_811_Fruit_Vegtable['image'] = imgRoot + 'Record Center Menus\\Food\\Apples.PNG'

    lunchDinner.NonInfant_Bread_Alt['quantity'] = '1'
    lunchDinner.NonInfant_Meat_Alt['quantity'] = '1'
    lunchDinner.NonInfant_Fruit_Juice_Veg['quantity'] = '1'
    lunchDinner.NonInfant_Fruit_Juice_Veg2['quantity'] = '1'
    lunchDinner.NonInfant_Milk['quantity'] = '1'
    lunchDinner.Infant_03_BreastMilk_Formula['quantity'] = '1'
    lunchDinner.Infant_47_BreastMilk_Formula['quantity'] = '1'
    lunchDinner.Infant_47_Infant_Cereal['quantity'] = '1'
    lunchDinner.Infant_47_Fruit_Vegtable['quantity'] = '1'
    lunchDinner.Infant_811_BreastMilk_Formula['quantity'] = '1'
    lunchDinner.Infant_811_Infant_Cereal_Meat_Alternative['quantity'] = '1'
    lunchDinner.Infant_811_Fruit_Vegtable['quantity'] = '1'

    lunchDinner.NonInfant_Bread_Alt['unitOfMeasure'] = 'ounces'
    lunchDinner.NonInfant_Meat_Alt['unitOfMeasure'] = 'ounces'
    lunchDinner.NonInfant_Fruit_Juice_Veg['unitOfMeasure'] = 'half pints'
    lunchDinner.NonInfant_Fruit_Juice_Veg2['unitOfMeasure'] = 'half pints'
    lunchDinner.NonInfant_Milk['unitOfMeasure'] = 'cups'
    lunchDinner.Infant_03_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    lunchDinner.Infant_47_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    lunchDinner.Infant_47_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    lunchDinner.Infant_47_Fruit_Vegtable['unitOfMeasure'] = 'slices'
    lunchDinner.Infant_811_BreastMilk_Formula['unitOfMeasure'] = 'cups'
    lunchDinner.Infant_811_Infant_Cereal_Meat_Alternative['unitOfMeasure'] = 'servings'
    lunchDinner.Infant_811_Fruit_Vegtable['unitOfMeasure'] = 'slices'

    lunchDinner.NonInfant_Bread_Alt['unitOfMeasureImageBool'] = '0'
    lunchDinner.NonInfant_Meat_Alt['unitOfMeasureImageBool'] = '0'
    lunchDinner.NonInfant_Fruit_Juice_Veg['unitOfMeasureImageBool'] = '0'
    lunchDinner.NonInfant_Fruit_Juice_Veg2['unitOfMeasureImageBool'] = '0'
    lunchDinner.NonInfant_Milk['unitOfMeasureImageBool'] = '0'
    lunchDinner.Infant_03_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    lunchDinner.Infant_47_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    lunchDinner.Infant_47_Infant_Cereal['unitOfMeasureImageBool'] = '0'
    lunchDinner.Infant_47_Fruit_Vegtable['unitOfMeasureImageBool'] = '0'
    lunchDinner.Infant_811_BreastMilk_Formula['unitOfMeasureImageBool'] = '0'
    lunchDinner.Infant_811_Infant_Cereal_Meat_Alternative['unitOfMeasureImageBool'] = '0'
    lunchDinner.Infant_811_Fruit_Vegtable['unitOfMeasureImageBool'] = '0'
    
    def __init__(self, month, day, year, meal_name, quantity_type):
        self.month = month
        self.day = day
        self.year = year
        self.meal_name = meal_name
        
        self.quantity_type = quantity_type
        if meal_name == 'Breakfast':
            self.meal = copy.copy(self.breakfast)
        elif meal_name == 'AMSnack':
            self.meal = copy.copy(self.snack)
        elif meal_name == 'PMSnack':
            self.meal = copy.copy(self.snack)
        elif meal_name == 'Snack':
            self.meal = copy.copy(self.snack)
        elif meal_name == 'EveningSnack':
            self.meal = copy.copy(self.snack)
        elif meal_name == 'Lunch':
            self.meal = copy.copy(self.lunchDinner)
        elif meal_name == 'Dinner':
            self.meal = copy.copy(self.lunchDinner)