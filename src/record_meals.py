class  Record_Meals:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth, propertyBag):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            i = 0
            self.selectRecordDailyAttendanceMealCounts()
            self.selectDayOfMonth(self.propertyBag.dayOfMonth)
            count = len(self.propertyBag.getMealList())
            
            while i<count:
                child=self.propertyBag.getMealList().pop()
                self.recordMeals(int(child.childNum),child.B,child.A,child.L,child.P,child.D,child.E)
                i=i+1
            
            self.saveMeals()
            self.verifyChanges()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def selectRecordDailyAttendanceMealCounts(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claim dialog
        sType(Key.RIGHT);
        sType(Key.DOWN * 9);
        sType(Key.ENTER); #opens record daily attendance/meal counts
    
    #Change the day of month
    def selectDayOfMonth(self,dayOfMonth):
        if dayOfMonth!="":
            sTypeModifyer(Key.TAB, KeyModifier.SHIFT)
            sType(Key.RIGHT)
            sType(dayOfMonth)
            
    #ChildNum - which child in the list to select (the 1st, the 2nd, the third, etc...)
    #Set the meals to 1 if you want to select them, anything else they will not be selected
    def recordMeals(self,childNum, B, A, L, P, D, E):        
        self.selectChild(childNum)
        
        if B==1:
            self.markBreakfast()
        if A==1:
            self.markAMSnack()
        if L==1:
            self.markLunch()
        if P==1:
            self.markPMSnack()
        if D==1:
            self.markDinner()
        if E==1:
            self.markEveningSnack()
            
        self.unSelectChild(childNum)
            
    def saveMeals(self):
        sTypeModifyer(Key.TAB, KeyModifier.SHIFT)
        wait(3)
        #Save changes
        if exists(imgRoot + "Record Meals\\saveYourChanges.PNG"):
            sType(Key.SPACE)
    
    #Move the cursor so that it selects any child on the list
    def selectChild(self, num):
        sType(Key.TAB * 17*(num-1));
        
    #Takes a known child position and moves your position back to the default position of child #1
    def unSelectChild(self,num):
        sType(Key.LEFT * 25)
        if num>1:
            #shiftTab((num-1)*17)
            sType(Key.UP *(num-1))
        
    def markBreakfast(self):
        sType(Key.TAB * 6)
        sType(Key.SPACE)
        shiftTab(6)
        
    def markAMSnack(self):
        sType(Key.TAB * 7)
        sType(Key.SPACE)
        shiftTab(7)
        
    def markLunch(self):
        sType(Key.TAB * 8)
        sType(Key.SPACE)
        shiftTab(8)
        
    def markPMSnack(self):
        sType(Key.TAB * 9)
        sType(Key.SPACE)
        shiftTab(9)
        
    def markDinner(self):
        sType(Key.TAB * 10)
        sType(Key.SPACE)
        shiftTab(10)
    
    def markEveningSnack(self):
        sType(Key.TAB * 11)
        sType(Key.SPACE)
        shiftTab(11)
    
    def verifyChanges(self):        
        assert wait(imgRoot + "Record Meals\\changesSaved.PNG",15)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def tearDown(self):
        sType(Key.SPACE)
        #Select the close key
        click(imgRoot + "Record Meals\\close.PNG",15)