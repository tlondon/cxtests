class Actual_VS_Estimate_Meal_Count_Summary:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectReport()
            HelperClass.verifyPDFFormOpened(self)
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectReport(self):
        sTypeModifyer("c", KeyModifier.ALT) #Claims
        sType(Key.RIGHT * 2) #Reports
        sType(Key.DOWN * 1) #Attendance
        sType(Key.RIGHT)
        sType(Key.DOWN * 13)
        
        sType(Key.ENTER) #Choose a report option
        sType(Key.ENTER) #Choose a report option

    def tearDown(self):
        closeWindow(1)