class Milk_Audit:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
    
    def runAll(self):
        try:
            self.selectReport()
            HelperClass.verifyPDFFormOpened(self)
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def selectReport(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claims dialog
        sType(Key.DOWN * 4)
        sType(Key.ENTER) #Select the Report
        
        shiftTab(11)
        sType(Key.SPACE)
            
    def tearDown(self):
        closeWindow(1)
        shiftTab(1)
        sType(Key.SPACE)
        