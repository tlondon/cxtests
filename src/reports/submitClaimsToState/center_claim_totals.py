class Center_Claim_Totals:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectSubmitClaimToState()
            self.verifyFormOpened()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectSubmitClaimToState(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claim dialog
        sType(Key.UP * 4)
        sType(Key.ENTER)
        sType(Key.TAB * 3)
        sType(Key.SPACE)
        sType(Key.SPACE)        
        
    def verifyFormOpened(self):               
        assert wait(imgRoot + 'Enrollment Form\\lots of icons.PNG', 45)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def tearDown(self):
        closeWindow(1)
        sType(Key.TAB*9)
        sType(Key.SPACE)