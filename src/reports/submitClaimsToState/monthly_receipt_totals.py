class Monthly_Receipt_Totals:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectSubmitClaimToState()
            self.verifyFormOpened()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectSubmitClaimToState(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claim dialog
        sType(Key.UP * 4)
        sType(Key.ENTER) #open Submit Claim to State dialog
        
        sType(Key.TAB*4)
        sType(Key.DOWN * 4) #Select the correct report
        
        shiftTab(1) #Select Print
        sType(Key.SPACE)        
        sType(Key.SPACE) #Select Print by Center
        
    def verifyFormOpened(self):               
        assert wait(imgRoot + 'Enrollment Form\\lots of icons.PNG', 45)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def tearDown(self):
        closeWindow(1)
        sType(Key.TAB*9)
        sType(Key.SPACE)