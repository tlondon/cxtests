class Center_List_Export:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectReport()
            HelperClass.verifyExcelFormOpened(self)
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectReport(self):
        sTypeModifyer("c", KeyModifier.ALT) #Claims
        sType(Key.RIGHT * 2) #Reports
        sType(Key.DOWN * 2) #Centers
        sType(Key.RIGHT)
        #sType(Key.DOWN * 1) #Select the Report
        sType(Key.ENTER) #Open the Report
        
        shiftTab(3)
        sType(Key.ENTER)
        sType(Key.TAB*104)
        sType(Key.SPACE)
        shiftTab(2)
        sType(Key.SPACE)
        
        sType(Key.ENTER) #Choose a report option
            
    def tearDown(self):
        closeWindow(1)