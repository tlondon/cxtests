class Claims_List_Export:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
    
    def runAll(self):
        try:
            self.selectReport()
            HelperClass.verifyExcelFormOpened(self)
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def selectReport(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claims dialog
        sType(Key.RIGHT * 2)
        sType(Key.DOWN * 5)
        sType(Key.RIGHT)
        #sType(Key.DOWN * 2)
        sType(Key.ENTER) #Select the Report
        
        shiftTab(3)
        sType(Key.ENTER)
        
        shiftTab(3)
        sType(Key.ENTER)
        
        shiftTab(2)
        sType(Key.SPACE)
        
        shiftTab(2)
        sType(Key.SPACE)
            
    def tearDown(self):
        closeWindow(1)