class  OER:
    free  = None
    reduced  = None
    paid = None
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectOER()
            self.selectMonth()
            self.printOER()
            self.obtainFRPNumbers()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
    
    def smoke(self):
        self.runAll()
        
    def selectOER(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the file dialog
        sType(Key.RIGHT * 2)
        sType(Key.DOWN * 5)
        sType(Key.RIGHT)
        sType(Key.DOWN * 3)
        sType(Key.ENTER) #Select the OER report
    
    def selectMonth(self):
        sType(Key.TAB)
        sType(Key.ENTER)
        
    def verifyReimbursementAmount(self):        
        assert wait(imgRoot + "OER\\1369423144422.png",30)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def printOER(self):        
        assert wait(imgRoot + "OER\\printOER.PNG",30)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
        
    def obtainFRPNumbers(self):
        curDay = datetime.date.today().strftime("%Y-%m-%d")
        curTime = datetime.datetime.now().strftime("%H-%M-%S")
        time = curDay + ' ' + curTime
        s = str(RunnerSettings.log.runNumber) #store this in a string
        HelperClass.takeScreenshot(RunnerSettings.load_config.errorScreenShotPath + s + '\\' + self.centerName + '_' +  'oer' + '_' + time + '.png')
        
        free = Region(1060, 215, 60, 11)
        freeEnd = Region(1015, 216, 65, 10)
        
        reduced = Region(1060, 230, 65, 13)
        reducedEnd = Region(1015, 229, 64, 12)
        
        paid = Region(1060, 245, 61, 14)
        paidEnd = Region(1015, 244, 61, 15)

        #Change the PDF to select text
        click(imgRoot + "OER\\text.PNG",30)
        
        #Obtain the frp numbers
        dragDrop(free, freeEnd)
        sTypeModifyer("c", KeyModifier.CTRL)
        self.free = Env.getClipboard().replace(" ", "")
        if self.free.isdigit()==False:
            raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to obtain OER free count.')
         
        HelperClass.writeFile('OER Free: ' + str(self.free))
        
        dragDrop(reduced, reducedEnd)
        sTypeModifyer("c", KeyModifier.CTRL)
        self.reduced = Env.getClipboard().replace(" ", "")
        if self.reduced.isdigit()==False:
            raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to obtain OER reduced count.')
        
        HelperClass.writeFile('OER Reduced: ' + str(self.reduced))
        
        dragDrop(paid, paidEnd)
        sTypeModifyer("c", KeyModifier.CTRL)
        self.paid = Env.getClipboard().replace(" ", "")
        if self.paid.isdigit()==False:
            raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to paid OER reduced count.')
        
        HelperClass.writeFile('OER Paid: ' + str(self.paid))
            
    def tearDown(self):
        closeWindow(1)