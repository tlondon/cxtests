class  Roster:
    free = None
    reduced = None
    paid = None
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
    
    def runAll(self):
        try:
            self.selectReport()
            self.selectMonth()
            HelperClass.verifyPDFFormOpened(self)
            self.obtainFRPNumbers()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
    
    def smoke(self):
        self.runAll()
        
    def selectReport(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claims dialog
        sType(Key.RIGHT * 2)
        sType(Key.DOWN * 3)
        sType(Key.RIGHT)
        sType(Key.ENTER) #Select the Child Roster report
    
    def selectMonth(self):
        sType(Key.ENTER*2)
        
    def obtainFRPNumbers(self):
        curDay = datetime.date.today().strftime("%Y-%m-%d")
        curTime = datetime.datetime.now().strftime("%H-%M-%S")
        time = curDay + ' ' + curTime
        s = str(RunnerSettings.log.runNumber) #store this in a string
        HelperClass.takeScreenshot(RunnerSettings.load_config.errorScreenShotPath + s + '\\' + self.centerName + '_' +  'oer' + '_' + time + '.png')
        
        free = Region(841, 160, 46, 12)
        freeEnd = Region(825, 150, 44, 12)
        
        reduced = Region(930, 162, 50, 12)
        reducedEnd = Region(910, 152, 53, 13)
        
        paid = Region(1003, 162, 44, 11)
        paidEnd = Region(985, 152, 43, 15)

        #Change the PDF to select text
        click(imgRoot + "OER\\text.PNG",30)
        
        #Obtain the frp numbers
        dragDrop(free, freeEnd)
        sTypeModifyer("c", KeyModifier.CTRL)
        self.free = Env.getClipboard().replace(" ", "")
        if self.free.isdigit()==False:
            raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to obtain Roster free count.')
        
        HelperClass.writeFile("Roster Free: " + str(self.free))
        
        dragDrop(reduced, reducedEnd)
        sTypeModifyer("c", KeyModifier.CTRL)
        self.reduced = Env.getClipboard().replace(" ", "")
        if self.reduced.isdigit()==False:
            raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to obtain Roster reduced count.')
        
        HelperClass.writeFile("Roster Reduced: " + str(self.reduced))
        
        dragDrop(paid, paidEnd)
        sTypeModifyer("c", KeyModifier.CTRL)
        self.paid = Env.getClipboard().replace(" ", "")
        if self.paid.isdigit()==False:
            raise Custom_Exception('Line: ' + getLineNumber() + '. Unable to paid Roster reduced count.')
        
        HelperClass.writeFile("Roster Paid: " + str(self.paid))
        
#        free2 = selectRegion()
#        print 'Free'
#        print '-------------------------------'
#        print 'X: ' + str(free2.getX())
#        print 'Y: ' + str(free2.getY())
#        print 'W: ' + str(free2.getW())
#        print 'H: ' + str(free2.getH())
#        
#        print ''
#        print ''
#        
#        reduced2 = selectRegion()
#        print 'X: ' + str(reduced2.getX())
#        print 'Y: ' + str(reduced2.getY())
#        print 'W: ' + str(reduced2.getW())
#        print 'H: ' + str(reduced2.getH())
#        
#        print '-------------------------------'
#        
#        
#        
#        free = selectRegion()
#        print 'Reduced'
#        print '-------------------------------'
#        print 'X: ' + str(free.getX())
#        print 'Y: ' + str(free.getY())
#        print 'W: ' + str(free.getW())
#        print 'H: ' + str(free.getH())
#        
#        print ''
#        print ''
#        
#        reduced = selectRegion()
#        print 'X: ' + str(reduced.getX())
#        print 'Y: ' + str(reduced.getY())
#        print 'W: ' + str(reduced.getW())
#        print 'H: ' + str(reduced.getH())
#        
#        print '-------------------------------'
#        print ''
#        
#        
#        
#        
#        free1 = selectRegion()
#        print 'Paid'
#        print '-------------------------------'
#        print 'X: ' + str(free1.getX())
#        print 'Y: ' + str(free1.getY())
#        print 'W: ' + str(free1.getW())
#        print 'H: ' + str(free1.getH())
#        
#        print ''
#        print ''
#        
#        reduced1 = selectRegion()
#        print 'X: ' + str(reduced1.getX())
#        print 'Y: ' + str(reduced1.getY())
#        print 'W: ' + str(reduced1.getW())
#        print 'H: ' + str(reduced1.getH())
#        
#        print '-------------------------------'
            
    def tearDown(self):
        closeWindow(1)