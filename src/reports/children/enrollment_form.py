class Enrollment_Form:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectEnrollmentForm()
            HelperClass.verifyPDFFormOpened(self)
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
            
    def selectEnrollmentForm(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claim dialog
        sType(Key.RIGHT * 2);
        sType(Key.DOWN * 3);
        sType(Key.RIGHT);
        sType(Key.DOWN * 3);
        sType(Key.RIGHT); #Select English
        sType(Key.ENTER)
            
    def tearDown(self):
        closeWindow(1)