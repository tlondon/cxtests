class Children_Claimed_Without_Abscence:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
    
    def runAll(self):
        try:
            self.selectReport()
            HelperClass.verifyPDFFormOpened(self)
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def selectReport(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claims dialog
        sType(Key.RIGHT * 2)
        sType(Key.DOWN * 3)
        sType(Key.RIGHT)
        sType(Key.DOWN * 9)
        sType(Key.ENTER) #Select the Report
        
        sType(Key.ENTER)
        sType(Key.ENTER)
            
    def tearDown(self):
        closeWindow(1)