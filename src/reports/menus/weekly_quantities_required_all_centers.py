class Weekly_Quantities_Required_All_Centers:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
    
    def runAll(self):
        try:
            self.selectReport()
            HelperClass.verifyPDFFormOpened(self)
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def selectReport(self):
        sTypeModifyer("c", KeyModifier.ALT) # opens the claims dialog
        sType(Key.RIGHT * 2)
        sType(Key.DOWN * 6)
        sType(Key.RIGHT)
        sType(Key.DOWN * 9)
        sType(Key.ENTER) #Select the Report
        
        shiftTab(3)
        sType(Key.ENTER)
        sType(Key.ENTER)
            
    def tearDown(self):
        closeWindow(1)