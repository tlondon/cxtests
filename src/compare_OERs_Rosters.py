class Compare_OERs_Rosters:
    def __init__(self, user, password, pathToCenter, centerName, claimMonth, processClaim):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.compareOERtoRoster()
        except:
            HelperClass.exceptionHandling(self)
            
    def compareOERtoRoster(self):
        
        if self.processClaim=='1':
            #Process Claim
            reProcess=1
            printOER=0
            useBlended=1
            t = Process_Claim(self.user, self.password, self.centerName, self.claimMonth, reProcess, printOER, useBlended)
            t.runAll()
        
        announce('TEST: OER')
        t1 = OER(self.user, self.password, self.centerName, self.pathToCenter, self.claimMonth)
        t1.runAll()

        announce('TEST: Roster')
        t2 = Roster(self.user, self.password, self.centerName, self.pathToCenter, self.claimMonth)
        t2.runAll()

        if t1.free != t2.free:
            raise Custom_Exception('Line: ' + getLineNumber() + '. OER and Roster Free Counts do not match. OER Count: ' + str(t1.free) + ' Roster Count: ' + str(t2.free))
        elif t1.reduced != t2.reduced:
            raise Custom_Exception('Line: ' + getLineNumber() + '. OER and Roster Reduced Counts do not match. OER Count: ' + str(t1.reduced) + ' Roster Count: ' + str(t2.reduced))
        elif t1.paid != t2.paid:
            raise Custom_Exception('Line: ' + getLineNumber() + '. OER and Roster Paid Counts do not match. OER Count: ' + str(t1.paid) + ' Roster Count: ' + str(t2.paid))

        #Passed
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)