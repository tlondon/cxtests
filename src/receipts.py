class  Receipts:
    
    def __init__(self, user, password, centerName, pathToCenter, claimMonth):
        HelperClass.initMethod(self,locals().keys(), locals().values(), user, password, centerName, pathToCenter, claimMonth)
        
    def runAll(self):
        try:
            self.selectReceipts()
            self.verifyReceipts()
            self.tearDown()
        except:
            HelperClass.exceptionHandling(self)
        
    def smoke(self):
        self.runAll()
            
    def selectReceipts(self):
        sTypeModifyer("f", KeyModifier.ALT) # opens the file dialog
        sType(Key.RIGHT * 6)
        sType(Key.DOWN * 2)
        sType(Key.ENTER)
        
    def verifyReceipts(self):        
        wait(imgRoot + "Receipts\\apple.PNG",15)
        HelperClass.passLogTestStatus(self, sys.argv[0], sys._getframe().f_code.co_name)
            
    def tearDown(self):
        sTypeModifyer("w", KeyModifier.CTRL) #Close browser window
        RunnerSettings.appHandle.focus('Minute Menu CX') #Return Focus to MM